# -- coding: UTF-8 --

"""
Log
====
The module which contains functions to build LOGGER and configure it.
"""

import json
import logging
from logging.config import dictConfig

__author__ = 'srivathsan@gyandata.com'


def build_logger(file):
    """
    A function to build a LOGGER from a JSON file

    :param file: a JSON file path for logging configuration
    :type file: str

    :return: the configured LOGGER
    :rtype: logging.Logger
    """
    with open(file, 'r') as f_open:
        # building LOGGER object
        log_config = json.load(f_open)
        dictConfig(log_config)
        logging.debug('Logger configured')
