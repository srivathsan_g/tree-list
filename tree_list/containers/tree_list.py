# -- coding: UTF-8 --

"""
Tree List
==========
This module contains the implementation of the Tree List class
"""

import logging

from tree_list.utils.error_handle import exception_handler, ERROR_IDX_TYPE, ERROR_IDX_VAL

__author__ = 'srivathsan@gyandata.com'

LOGGER = logging.getLogger(__name__)


class TreeList(list):
    """
    Class representing a Tree List where a list is stored as an AVL Tree to make sure insertions and deletions
    are in O(logn). Inherits list

    :ivar left: the left node of the tree list.
    :vartype left: :class: `tree_list.containers.tree_list.TreeList`

    :ivar right: the right node of the tree list
    :vartype right: :class: `tree_list.containers.tree_list.TreeList`

    :ivar idx: the index of the object which is inserted
    :vartype idx: int

    :ivar obj: the object to be inserted at idx
    :vartype obj: object
    """

    def __init__(self):

        super().__init__()
        self.left = None
        self.right = None
        self.idx = None
        self.obj = None
        self._mod_count = 0

    def __str__(self):
        """
        Method overriding the string representation of the class

        :return: the string representation of the list
        :rtype: str
        """
        return str(self.to_array())

    def __len__(self):
        """
        Method overriding the length

        :return: the length of the tree list
        :rtype: int
        """
        arr = self.to_array()

        # if list is none return 0
        return len(arr) if arr else 0

    def __contains__(self, obj):
        """
        Method overriding the usage of in operator

        :param obj: the object to be checked if present in the list
        :type obj: object

        :return: true if present else false
        :rtype: bool
        """
        return False if self.index_of(obj) is None else True

    def __iter__(self):
        """
        Method overriding the iterator of the list

        :return: the iterator of the list
        :rtype: iterable
        """
        return iter(self.to_array())

    def __getitem__(self, slice_idx):
        """
        Method overriding the get item of the list

        :param slice_idx: the slicing indices
        :type slice_idx: slice

        :return: the sliced list
        :rtype: list
        """
        return self.to_array()[slice_idx]

    @exception_handler
    def index_of(self, obj):
        """
        Method to find the index of given object

        :param obj: the object which index must be found
        :type obj: object

        :return: the index of the object if it exists else None
        :rtype: int
        """

        # check if the object is in the current node, if yes return idx
        if self.obj == obj:
            return self.idx

        # traverse the left side in dfs fashion to find the index
        idx = self.left.index_of(obj) if self.left else None

        # if still idx is not found traverse right side
        if idx is None:
            idx = self.right.index_of(obj) if self.right else None

        return idx

    def inorder(self, arr=[]):
        """
        Method to return a list of the order traversal of the tree

        :param arr: the array which will contain the inorder traversal of the tree
        :type arr: list

        :return: the inorder traversal
        :rtype: list
        """

        # if current idx is none return
        if self.idx is None:
            return None

        # if left exists append left subtree objects
        if self.left:
            arr.extend(self.left.inorder([]))

        # append current object
        arr.append(self.obj)

        # if right exists append right subtree objects
        if self.right:
            arr.extend(self.right.inorder([]))

        return arr

    def to_array(self):
        """
        Method to return the tree as list. The inorder traversal of the tree provides the elements in insertion order

        :return: the list of elements
        :rtype: list
        """
        return self.inorder([])

    @exception_handler
    def get_tree(self, level=0):
        """
        Method which recursively calls itself to get the elements of the tree list in a tree-like fashion

        :param level: the depth of the self node
        :type level: int

        :return: the string representation of the tree
        :rtype: str
        """
        # the indentation is determined by how deep the tree is
        ret = "\t" * level + f'{self.obj}({self.idx})\n'

        # recursive call to print the children
        if self.left:
            ret += self.left.get_tree(level + 1)

        if self.right:
            ret += self.right.get_tree(level + 1)

        return ret

    @exception_handler
    def get(self, idx):
        """
        Method to get the object at the given index

        :param idx: the index from which object must be obtained
        :type idx: int

        :return: the object at idx
        :rtype: object
        """

        # parameter check
        if not issubclass(type(idx), int):
            raise TypeError(ERROR_IDX_TYPE)
        if idx < 0 or idx > len(self):
            raise ValueError(ERROR_IDX_VAL)

        # if idx does not exist return None
        if self.idx is None:
            return None

        # else if found return object
        if self.idx == idx:
            return self

        # if idx is less than current traverse left
        if idx < self.idx:
            return self.left.get(idx) if self.left else None

        # else traverse right
        return self.right.get(idx) if self.right else None

    @exception_handler
    def set(self, idx, obj):
        """
        Method to set the object at a given index

        :param idx: the index at which the object must be set
        :type idx: int

        :param obj: the object to be set
        :type obj: object

        :return: None
        :rtype: None
        """

        # get the node at that index
        node = self.get(idx)

        # set the object at that node
        node.obj = obj

    def height(self):
        """
        Method to find the height of the given node (depth)

        :return: height of the node
        :rtype: int
        """

        # if current node does not exist return 0
        if self.idx is None:
            return 0

        # if current node is leaf return 1
        if self.left is None and self.right is None:
            return 1

        # if left subtree does not exist return height of right subtree + 1
        if self.left is None:
            return self.right.height() + 1

        # if right subtree does not exist return height of left subtree + 1
        if self.right is None:
            return self.left.height() + 1

        # else find max height of left and right subtree and return that + 1
        return max(self.left.height(), self.right.height()) + 1

    def balance_factor(self):
        """
        Method to find the balance factor of current node. balance factor is height of left subtree- right

        :return: balance factor of given node
        :rtype: int
        """

        # if current node is none or is a leaf node return 0
        if self.idx is None or (self.left is None and self.right is None):
            return 0

        # if left node is none return -height of right
        if self.left is None:
            return -self.right.height()

        # if right is none return height of left
        if self.right is None:
            return self.left.height()

        # else return difference of left and right
        return self.left.height() - self.right.height()

    def rot_left(self):
        """
        Method to rotate left the current node

        :return: the rotated node
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # store the right and right's left node
        temp_right = self.right
        temp_left = temp_right.left

        # change the right's left node and the right node
        temp_right.left = self
        self.right = temp_left

        return temp_right

    def rot_right(self):
        """
        Method to rotate right the current node

        :return: the rotated node
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """
        # store the left and left's right node
        temp_left = self.left
        temp_right = temp_left.right

        # change the left's right node and the left node
        temp_left.right = self
        self.left = temp_right

        return temp_left

    def add(self, idx, obj):
        """
        Method to insert an element in the Tree List by using AVL tree insertion algorithm based on index

        :param idx: the index at which object must be inserted
        :type idx: int

        :param obj: the object to be inserted
        :type obj: object

        :return: the Tree list with obj inserted at idx
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # parameter check
        if not issubclass(type(idx), int):
            raise TypeError(ERROR_IDX_TYPE)
        if idx < 0:
            raise ValueError(ERROR_IDX_VAL)

        # change mod count of the current node
        self._mod_count += 1

        # if current node is none insert the object
        if self.idx is None:
            self.idx = idx
            self.obj = obj
            return self

        # if idx is less than current index traverse left
        if idx < self.idx:
            if self.left is None:
                self.left = TreeList()
            self.left = self.left.add(idx, obj)

        # else traverse right
        else:
            if self.right is None:
                self.right = TreeList()
            self.right = self.right.add(idx, obj)

        # get the balance factor of current node
        bal = self.balance_factor()

        # left-left case
        if bal > 1 and idx < self.left.idx:
            return self.rot_right()

        # right-right case
        if bal < -1 and idx > self.right.idx:
            return self.rot_left()

        # left-right case
        if bal > 1 and idx > self.left.idx:
            self.left = self.left.rot_left()
            return self.rot_right()

        # right-left case
        if bal < -1 and idx < self.right.idx:
            self.right = self.right.rot_right()
            return self.rot_left()

        return self

    @exception_handler
    def add_all(self, objs):
        """
        Method to add a list of objects

        :param objs: the list of objects
        :type objs: list or tuple

        :return: the root node with objs added
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # parameter check
        if not issubclass(type(objs), (list, tuple)):
            raise TypeError(ERROR_IDX_TYPE)

        # obtain the current length of the list
        arr_len = len(self)

        # start the index from the next index of length and add each obj
        for idx, obj in enumerate(objs, arr_len):
            LOGGER.debug('Adding element at index %d', idx)
            self = self.add(idx, obj)

        LOGGER.debug(self)

        return self

    def get_min_val(self):
        """
        Method to get the node with minimum value(index)

        :return: the node with minimum index
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # recursively traverse the left subtree to obtain the minimum node
        if self is None or self.left is None:
            return self

        return self.left.get_min_val()

    def remove(self, idx):
        """
        Method to remove the node at the given index by using AVL tree deletion algorithm based on index

        :param idx: the node at the index to be removed
        :type idx: int

        :return: the current node with idx removed
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # parameter check
        if not issubclass(type(idx), int):
            raise TypeError(ERROR_IDX_TYPE)

        # increase mod count
        self._mod_count += 1

        # if current node is none return
        if self.idx is None:
            self.obj = None
            return self

        # if idx is less than current index traverse left
        if idx < self.idx:
            if self.left is None:
                return self
            self.left = self.left.remove(idx)

        # if idx is greater than current index traverse right
        if idx > self.idx:
            if self.right is None:
                return self
            self.right = self.right.remove(idx)

        # else if idx is same as current idx

        # if no left subtree return right subtree
        if self.left is None:
            temp = self.right
            self = None
            return temp

        # if no right subtree return left subtree
        if self.right is None:
            temp = self.left
            self = None
            return temp

        # if both subtrees exist find the minimum node in the right subtree
        temp = self.right.get_min_val()

        # make that node as the current node
        self.idx = temp.idx
        self.obj = temp.obj

        # make the current node's right subtree as removal of the minimum node previously obtained
        self.right = self.right.remove(temp.idx)

        # if only one node exists return current
        if self.idx is None:
            self.obj = None
            return self

        # get the balance factor of current node
        bal = self.balance_factor()

        # left-left case based on balance factor of left node
        if bal > 1 and self.left.balance_factor() >= 0:
            return self.rot_right()

        # right-right case based on balance factor of right node
        if bal < -1 and self.right.balance_factor() <= 0:
            return self.rot_left()

        # right-left case based on balance factor of left node
        if bal > 1 and self.left.balance_factor() < 0:
            self.left = self.left.rot_left()
            return self.rot_right()

        # left-right case based on balance factor of right node
        if bal < -1 and self.right.balance_factor() > 0:
            self.right = self.right.rot_right()
            return self.rot_left()

        return self

    @exception_handler
    def clear(self):
        """
        Method to clear all elements of the array

        :return: the object with all elements removed
        :rtype: :class: `tree_list.containers.tree_list.TreeList`
        """

        # obtain len of current list
        arr_len = len(self)

        # traverse the list and for each index perform removal
        for idx in range(arr_len):
            LOGGER.debug('Removing element at index %d', idx)
            self = self.remove(idx)

        # set current node to None
        self.obj = None

        return self
