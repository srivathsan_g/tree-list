# -- coding: UTF-8 --

"""
Tree List Main
==========
main script to test tree list
"""
import logging

from tree_list.containers.tree_list import TreeList
from tree_list.utils.log import build_logger

__author__ = "srivathsan@gyandata.com"

LOG_CONFIG = "configs/log_config.json"

LOGGER=logging.getLogger(__name__)


def main():
    """
    main function
    """
    build_logger(LOG_CONFIG)

    tree1=TreeList()
    tree1=tree1.add(0, 1)
    tree1=tree1.add(1, 2)
    LOGGER.debug(tree1.to_array())
    tree1=tree1.add(2, 3)
    LOGGER.debug(tree1.to_array())

    tree1=tree1.add_all([4, 5, 6, 7, 8, 9, 10, 11, 12])

    tree1.set(0, 5)
    LOGGER.debug(tree1.to_array())

    LOGGER.debug("\n"+tree1.get_tree())

    tree1=tree1.remove(7)
    LOGGER.debug(tree1)
    LOGGER.debug("\n"+tree1.get_tree())

    LOGGER.debug(10 in tree1)

    for i in tree1:
        LOGGER.debug(i)

    LOGGER.debug(tree1[::-1])

    tree1=tree1.clear()


if __name__ == '__main__':
    main()
