******
Usage
******

This package aims at implementing the `TreeList
<https://commons.apache.org/proper/commons-collections/apidocs/org/apache/commons/collections4/list/TreeList.html>`_
from Java Apache libraries in python.
A List implementation that is optimised for fast insertions and removals at any index in the list.
This list implementation utilises a tree structure (*AVL Tree*) internally to ensure that all insertions and removals
are O(log n).
This provides much faster performance than both an `ArrayList
<https://docs.oracle.com/javase/7/docs/api/java/util/ArrayList.html>`_ and a `LinkedList
<https://docs.oracle.com/javase/7/docs/api/java/util/LinkedList.html>`_ , from Java,  where elements are inserted
and removed repeatedly from anywhere in the list.

The following relative performance statistics are indicative of this class (from Java):

 =========== ==== ===== ======= ======== ======
  class       get  add  insert  iterate  remove
 =========== ==== ===== ======= ======== ======
 TreeList       3    5       1       2       1
 ArrayList      1    1      40       1      40
 LinkedList  5800    1     350       2     325
 =========== ==== ===== ======= ======== ======


Examples
**********

Here we will discuss some examples on how to use this package

To create a *TreeList* call the class and insert objects and their corresponding indices using the
:func:`~tree_list.containers.tree_list.TreeList.add` method. This method returns the root node of the tree constructed
based on the elements inserted. The :func:`~tree_list.containers.tree_list.TreeList.to_array` method can be
called to obtain the elements in a list-like format.

>>> from tree_list.containers.tree_list import TreeList
>>> tree1 = TreeList()
>>> tree1 = tree1.add(0, 1)
>>> tree1 = tree1.add(1, 2)
>>> tree1 = tree1.add(2, 3)
>>> print(tree1.to_array())
[1, 2, 3]

The :func:`~tree_list.containers.tree_list.TreeList.add_all` method can be used to append all objects from an iterable.

>>> tree1 = tree1.add_all([4, 5, 6, 7, 8, 9, 10, 11, 12])
>>> print(tree1.to_array())
[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

The :func:`~tree_list.containers.tree_list.TreeList.get` method returns the node corresponding to the index given.

>>> print(tree1.get(0))

The :func:`~tree_list.containers.tree_list.TreeList.set` method can be used to set the element at a given index.

>>> tree1.set(0, 5)
>>> print(tree1)
[5, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]

The :func:`~tree_list.containers.tree_list.TreeList.remove` method removes the element at the given index from the list. It returns the node corresponding to the
root of the tree after removing the element

>>> tree1 = tree1.remove(7)
>>> print(tree1)
[5, 2, 3, 4, 5, 6, 7, 9, 11, 12]

The :func:`~tree_list.containers.tree_list.TreeList.clear` method removes all elements from the list.

>>> tree1 = tree1.clear()
>>> print(tree1)
[None]


